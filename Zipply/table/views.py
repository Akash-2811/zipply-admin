from django.shortcuts import render, redirect

# Create your views here.
def bootstrap(request):
    return render(request, 'table/table-bootstrap-basic.html')

def database(request):
    return render(request, 'table/table-datatable-basic.html')