from django.urls import path
from . import views

urlpatterns = [
    path('bootstrap', views.bootstrap, name='table-bootstrap-basic'),
    path('database', views.database, name='table-datatable-basic'),
]