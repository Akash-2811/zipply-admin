from django.urls import path
from . import views

urlpatterns = [
    path('error_400', views.error_400, name='page-error-400'),
    path('error_403', views.error_403, name='page-error-403'),
    path('error_404', views.error_404, name='page-error-404'),
    path('error_500', views.error_500, name='page-error-500'),
    path('error_503', views.error_503, name='page-error-503'),
]