from django.shortcuts import render, redirect

# Create your views here.
def error_400(request):
    return render(request, 'error/page-error-400.html')

def error_403(request):
    return render(request, 'error/page-error-403.html')

def error_404(request):
    return render(request, 'error/page-error-404.html')

def error_500(request):
    return render(request, 'error/page-error-500.html')

def error_503(request):
    return render(request, 'error/page-error-503.html')