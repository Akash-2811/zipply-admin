from django.urls import path
from . import views

urlpatterns = [
     path('', views.index, name='index'),
     path('analytics', views.analytics, name='page-analytics'),
     path('review', views.review, name='page-review'),
     path('order', views.order, name='page-order'),
     path('order-list', views.order_list, name='page-order-list'),
     path('customers', views.customers, name='page-general-customers'),
]