from django.shortcuts import render, redirect

# Create your views here.
def index(request):
    return render(request, 'index/index.html')

def analytics(request):
    return render(request, 'index/page-analytics.html')

def review(request):
    return render(request, 'index/page-review.html')

def order(request):
    return render(request, 'index/page-order.html')

def order_list(request):
    return render(request, 'index/page-order-list.html')

def customers(request):
    return render(request, 'index/page-general-customers.html')