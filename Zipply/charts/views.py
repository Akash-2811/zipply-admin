from django.shortcuts import render, redirect

# Create your views here.
def flot(request):
    return render(request, 'charts/chart-popover.html')

def morris(request):
    return render(request, 'charts/chart-morris.html')

def chartjs(request):
    return render(request, 'charts/chart-chartjs.html')

def chartist(request):
    return render(request, 'charts/chart-chartist.html')

def sparkline(request):
    return render(request, 'charts/chart-sparkline.html')

def peity(request):
    return render(request, 'charts/chart-peity.html')