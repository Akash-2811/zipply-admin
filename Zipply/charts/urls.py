from django.urls import path
from . import views

urlpatterns = [
    path('flot', views.flot, name='chart-flot'),
    path('morris', views.morris, name='chart-morris'),
    path('chartjs', views.chartjs, name='chart-chartjs'),
    path('chartist', views.chartist, name='chart-chartist'),
    path('sparkline', views.sparkline, name='chart-sparkline'),
    path('peity', views.peity, name='chart-peity'),
]