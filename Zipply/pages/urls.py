from django.urls import path
from . import views
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('register', views.register, name='page-register'),
    path('login', views.login, name='page-login'),
    path('error/', include('error.urls')),
    path('lock_screen', views.lock_screen, name='page-lock-screen'),
    path('forgot-password', views.forgot_password, name='page-forgot-password'),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)