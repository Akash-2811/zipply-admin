from django.shortcuts import render, redirect

# Create your views here.
def register(request):
    return render(request, 'pages/page-register.html')

def login(request):
    return render(request, 'pages/page-login.html')

def lock_screen(request):
    return render(request, 'pages/page-lock-screen.html')

def forgot_password(request):
    return render(request, 'pages/page-forgot-password.html')
