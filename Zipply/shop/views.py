from django.shortcuts import render, redirect

# Create your views here.
def product_grid(request):
    return render(request, 'shop/ecom-product-grid.html')

def product_list(request):
    return render(request, 'shop/ecom-product-list.html')

def product_details(request):
    return render(request, 'shop/ecom-product-detail.html')

def order(request):
    return render(request, 'shop/ecom-product-order.html')

def checkout(request):
    return render(request, 'shop/ecom-checkout.html')

def invoice(request):
    return render(request, 'shop/ecom-invoice.html')

def customers(request):
    return render(request, 'shop/ecom-customers.html')