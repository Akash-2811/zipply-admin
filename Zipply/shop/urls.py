from django.urls import path
from . import views

urlpatterns = [
    path('product_grid', views.product_grid, name='ecom-product-grid'),
    path('product_list', views.product_list, name='ecom-product-list'),
    path('product_details', views.product_details, name='ecom-product-detail'),
    path('order', views.order, name='ecom-product-order'),
    path('checkout', views.checkout, name='ecom-checkout'),
    path('invoice', views.invoice, name='ecom-invoice'),
    path('customers', views.customers, name='ecom-customers'),
]