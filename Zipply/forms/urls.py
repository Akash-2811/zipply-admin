from django.urls import path
from . import views

urlpatterns = [
    path('form_elements', views.form_elements, name='form-element'),
    path('wizard', views.wizard, name='form-wizard'),
    path('summernote', views.summernote, name='form-editor-summernote'),
    path('pickers', views.pickers, name='form-pickers'),
    path('jquery_validate', views.jquery_validate, name='form-validation-jquery'),
]