from django.shortcuts import render, redirect

# Create your views here.
def form_elements(request):
    return render(request, 'forms/form-element.html')

def wizard(request):
    return render(request, 'forms/form-wizard.html')

def summernote(request):
    return render(request, 'forms/form-editor-summernote.html')

def pickers(request):
    return render(request, 'forms/form-pickers.html')

def jquery_validate(request):
    return render(request, 'forms/form-validation.html')