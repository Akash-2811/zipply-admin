from django.urls import path
from . import views

urlpatterns = [
    path('select2', views.select2, name='uc-select2'),
    path('nestedable', views.nestedable, name='uc-nestable'),
    path('noui_slider', views.noui_slider, name='uc-noui-slider'),
    path('sweet_alert', views.sweet_alert, name='uc-sweetalert'),
    path('toastr', views.toastr, name='uc-toastr'),
    path('jqv_map', views.jqv_map, name='map-jqvmap'),
]