from django.shortcuts import render, redirect

# Create your views here.
def select2(request):
    return render(request, 'plugins/uc-select2.html')

def nestedable(request):
    return render(request, 'plugins/uc-nestable.html')


def noui_slider(request):
    return render(request, 'plugins/uc-noui-slider.html')


def sweet_alert(request):
    return render(request, 'plugins/uc-sweetalert.html')


def toastr(request):
    return render(request, 'plugins/uc-toastr.html')


def jqv_map(request):
    return render(request, 'plugins/map-jqvmap.html')
