from django.shortcuts import render

# Create your views here.
def compose(request):
    return render(request, 'emails/email-compose.html')

def inbox(request):
    return render(request, 'emails/email-inbox.html')

def read(request):
    return render(request, 'emails/email-read.html')