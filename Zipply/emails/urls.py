from django.urls import path
from . import views

urlpatterns = [
    path('compose', views.compose, name='email-compose'),
    path('inbox', views.inbox, name='email-inbox'),
    path('read', views.read, name='email-read'),
]