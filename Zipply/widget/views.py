from django.shortcuts import render, redirect

# Create your views here.
def widget(request):
    return render(request, 'widget/widget-basic.html')