from django.urls import path
from . import views

urlpatterns = [
    path('', views.widget, name='widget-basic'),
]