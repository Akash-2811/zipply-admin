from django.urls import path
from . import views

urlpatterns = [
    path('accordion', views.accordion, name='ui-accordion'),
    path('alert', views.alert, name='ui-alert'),
    path('badge', views.badge, name='ui-badge'),
    path('button', views.button, name='ui-button'),
    path('card', views.card, name='ui-card'),
    path('modal', views.modal, name='ui-modal'),
    path('button_group', views.button_group, name='ui-button-group'),
    path('list_group', views.list_group, name='ui-list-group'),
    path('media_object', views.media_object, name='ui-media-object'),
    path('cards', views.cards, name='ui-cards'),
    path('carousel', views.carousel, name='ui-carousel'),
    path('dropdown', views.dropdown, name='ui-dropdown'),
    path('popover', views.popover, name='ui-popover'),
    path('progressbar', views.progressbar, name='ui-progressbar'),
    path('tab', views.tab, name='ui-tab'),
    path('typography', views.typography, name='ui-typography'),
    path('pagination', views.pagination, name='ui-pagination'),
    path('grid', views.grid, name='ui-grid'),
]