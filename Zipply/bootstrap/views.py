from django.shortcuts import render, redirect

# Create your views here.
def accordion(request):
    return render(request, 'bootstrap/ui-accordion.html')

def alert(request):
    return render(request, 'bootstrap/ui-alert.html')

def badge(request):
    return render(request, 'bootstrap/ui-badge.html')

def button(request):
    return render(request, 'bootstrap/ui-button.html')

def card(request):
    return render(request, 'bootstrap/ui-card.html')

def modal(request):
    return render(request, 'bootstrap/ui-modal.html')

def button_group(request):
    return render(request, 'bootstrap/ui-button-group.html')

def list_group(request):
    return render(request, 'bootstrap/ui-list-group.html')

def media_object(request):
    return render(request, 'bootstrap/ui-media-object.html')

def cards(request):
    return render(request, 'bootstrap/ui-card.html')

def carousel(request):
    return render(request, 'bootstrap/ui-carousel.html')

def dropdown(request):
    return render(request, 'bootstrap/ui-dropdown.html')

def popover(request):
    return render(request, 'bootstrap/ui-popover.html')

def progressbar(request):
    return render(request, 'bootstrap/ui-progressbar.html')

def tab(request):
    return render(request, 'bootstrap/ui-tab.html')

def typography(request):
    return render(request, 'bootstrap/ui-typography.html')

def pagination(request):
    return render(request, 'bootstrap/ui-pagination.html')

def grid(request):
    return render(request, 'bootstrap/ui-grid.html')