from django.shortcuts import render, redirect

# Create your views here.
def profile(request):
    return render(request, 'apps/app-profile.html')

def calendar(request):
    return render(request, 'apps/app-calender.html')