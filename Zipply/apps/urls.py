from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
    path('', views.profile, name='app-profile'),
    path('calender', views.calendar, name='app-calender'),
    path('emails/', include('emails.urls')),
    path('shop/', include('shop.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)