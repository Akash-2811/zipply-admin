from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('index.urls')),
    path('apps/', include('apps.urls')),
    path('charts/', include('charts.urls')),
    path('bootstrap/', include('bootstrap.urls')),
    path('plugins/', include('plugins.urls')),
    path('widget/', include('widget.urls')),
    path('forms/', include('forms.urls')),
    path('table/', include('table.urls')),
    path('pages/', include('pages.urls')),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
